import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Login from '../Login';
import Register from '../Register';
import Main from '../Main';

const App =() => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/main" component={Main} />
      </Switch>
    </Router>
  );
}

export default App;
