import React from "react";
import {
  Link,
  useHistory,
} from 'react-router-dom';
import * as apiUtils from '../../utils/apiUtils';

 const Register = () => {
  const [state, setState] = React.useState({
    email: "",
    username: "",
    password: "",
  });
  const [emailValid, setEmailValid] = React.useState(false);

  const history = useHistory();

  const handleChange = (e) =>{
    const value = e.target.value;
    setState({
      ...state,
      [e.target.name]: value
    });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = state.email;
    const user_name = state.username;
    const password = state.password;
    try {
      const registerRes = await apiUtils.register({ email, user_name, password });
      if (registerRes.status === 200) {
        history.push('/login')
      }
    } catch (error) {
      if (error.response.status === 404) {
        setEmailValid(true);
      }
    }
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label>
          Email:
          <input
            type="text"
            name="email"
            value={state.email}
            onChange={handleChange}
          />
        </label>
        {emailValid ? <div style={{color: 'red'}}>The email already exists</div> : null}
        <label>
          Username:
          <input
            type="text"
            name="username"
            value={state.username}
            onChange={handleChange}
          />
        </label>
        <label>
          Password:
          <input
            type="text"
            name="password"
            value={state.password}
            onChange={handleChange}
          />
        </label>
        <button >Register</button>
      </form>
      <Link to='/login'><button>Back</button></Link>
    </div>
  );
}

export default Register;
