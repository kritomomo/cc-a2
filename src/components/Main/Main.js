import React, { useState, useEffect } from 'react';
import {
  Link
} from 'react-router-dom';
import * as apiUtils from '../../utils/apiUtils';
import Subscription from './components/Subscription';
import Query from './components/Query';
import './Main.css';

const Main = () => {
  const [name, setName] = useState({
    username: ''
  })

  const [show, setShow] = useState(0);


  const getUserName = async () => {
    const email = localStorage.getItem('email');
    const usernameRes = await apiUtils.getName(email);
    if (usernameRes.status === 200) {
      setName({ username: usernameRes.data })
    }
  }

  useEffect(() => {
    getUserName();
  }, []);



  const { username } = name;


  return (
    <div className="main-layout">
      <h3 className="username">{username}</h3>
      <div>
        <h3>Subscription Area</h3>
        <Subscription show={show} setShow={setShow} />
      </div>
      <div className="subscription"></div>
      <div>
        <h3>Query Area</h3>
        <Query  show={show} setShow={setShow} />
      </div>
      <div className="query"></div>
      <div className="logout"></div>
      <Link to='/login'>Logout</Link>
    </div>
  )
}

export default Main;
