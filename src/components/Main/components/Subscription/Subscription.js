import React, { useState, useEffect } from 'react';
import * as apiUtils from '../../../../utils/apiUtils';
import './Subscription.css';
// import SubscriptionItem from './SubscriptionItem';

const Subscription = ({show, setShow}) => {
  const [subscriptionInfo, setSubscriptionInfo] = useState({
    subscriptionList: [],
  });



  const getSubscription = async () => {
    const email = localStorage.getItem('email');
    const subscriptionRes = await apiUtils.getSubscriptionDetails(email);
    // const artist = subscriptionRes.data.artist;
    // const artistImgRes = await apiUtils.getArtistImg(artist);
    try {
      if (subscriptionRes.status === 200) {
        setSubscriptionInfo({
          subscriptionList: subscriptionRes.data,
          
        })
        console.log(subscriptionList);
      }
    } catch (error) {
      if (error.response.status === 404) {
        setSubscriptionInfo({
          subscriptionList:[],
        })
      }
    }
  }

  useEffect(() => {

    getSubscription()
  }, [show])
  
  // useInterval(() => {
  //   getSubscription()
  // }, 3000);

  // function useInterval(callback, delay) {
  //   const savedCallback = React.useRef();
  //   useEffect(() => {
  //     savedCallback.current = callback;
  //   }, [callback]);
  //   useEffect(() => {
  //     function tick() {
  //       savedCallback.current();
  //     }
  //     if (delay !== null) {
  //       let id = setInterval(tick, delay);
  //       return () => clearInterval(id);
  //     }
  //   }, [delay]);
  // }

//   function encode(filename) {
//     const encodings = {
//         '\+': "%2B",
//         '\!': "%21",
//         '\"': "%22",
//         '\#': "%23",
//         '\$': "%24",
//         '\&': "%26",
//         '\'': "%27",
//         '\(': "%28",
//         '\)': "%29",
//         '\*': "%2A",
//         '\,': "%2C",
//         '\:': "%3A",
//         '\;': "%3B",
//         '\=': "%3D",
//         '\?': "%3F",
//         '\@': "%40",
//     };

//     return filename.replace(
//         /([+!"#$&'()*+,:;=?@])/img,
//         match => encodings[match]
//     );
// }
  
  // useEffect(() => {
  //   getSubscription(); const interval = setInterval(() => {
  //     getSubscription()
    
  //   }, 20000); return() => clearInterval(interval)
  // },[]);


  // const remove = index => {
    // console.log(index);
    // const newSubscription = [...subscriptionInfo];
    // newSubscription.splice(index, 1);
    // setSubscriptionInfo(newSubscription);
  // };

  const handleRemove = async (result,index) => {
    const title = result.title;
    // const years = result.years;
    // const artist = result.artist;
    const email = localStorage.getItem("email");
    // const artistImgRes = await apiUtils.getArtistImg(artist);
    // const image_url = artistImgRes.data;
    console.log(title,index);
    const newSubscription = [...subscriptionList];
    newSubscription.splice(index, 1)
    setSubscriptionInfo({
      subscriptionList:newSubscription,
    })
    // const email = localStorage.getItem('email');
    apiUtils.deleteSubscription({ title,email })
    // const f = await apiUtils.getSubscriptionDetails(email);
    // if (r.status === 200) {
      
      
    // }
  }


  const { subscriptionList } = subscriptionInfo;

  // const subscriptionDisplay = subscriptionList[0] ? (subscriptionList.map((e) => <SubscriptionItem key={e} result={e} />)) : <div></div>

  // const subscriptionDisplay = subscriptionList[0] ? (subscriptionList.map((result) =>
  //   <div>
  //     <tr>
  //       <td>{subscriptionList.title}</td>
  //       <td>{subscriptionList.years}</td>
  //       <td>{subscriptionList.artist}</td>
  //       <td><img src={subscriptionList.image_url} alt={subscriptionList.artist} /></td>
  //       <button onClick={() => handleRemove(result)}>Remove</button>
  //     </tr>
  //   </div>
  // )) : <div></div>

  return (
    <div>
      <div className="sub-area">
        <table>
          <tr>
            <th>Title</th>
            <th>Artist</th>
            <th>Year</th>
            <th>Artist image</th>
          </tr>
          {/* {subscriptionDisplay} */}
          {subscriptionList.map((result,index) => (
            <tr>
              <td>{result.title}</td>
              <td>{result.years}</td>
              <td>{result.artist}</td>
              <td><img src={result.image_url} alt={result.artist} /></td>
              <button onClick={() => handleRemove(result,index)}>Remove</button>
            </tr>
          ))}
        </table>
      </div>
    </div>
  )
}

export default Subscription;

