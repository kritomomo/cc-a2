import React from 'react';
import * as apiUtils from '../../../../utils/apiUtils';

const SubscriptionItem = ({ result }) => {
  const handleRemove = async (result) => {
    const title = result.title;
    // const years = result.years;
    // const artist = result.artist;
    // const email = localStorage.getItem("email");
    // const artistImgRes = await apiUtils.getArtistImg(artist);
    // const image_url = artistImgRes.data;
    console.log(title);
    const email = localStorage.getItem('email');
    const r = await apiUtils.deleteSubscription({ title })
    const f = await apiUtils.getSubscriptionDetails(email);
    if (r.status === 200) {
      
      
    }
  }
  
  const { title, artist, years,image_url } = result;


  return (
    <tr>
        <td>{title}</td>
        <td>{years}</td>
        <td>{artist}</td>
        <td><img src={image_url} alt={artist} /></td>
        <button onClick={() => handleRemove(result)}>Remove</button>
      </tr>
  )
  
}

export default SubscriptionItem;
