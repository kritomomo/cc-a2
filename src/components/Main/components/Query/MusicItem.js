import * as apiUtils from '../../../../utils/apiUtils';

const MusicItem = ({ result }) => {
  
  const handleSubscribe = async (result) => {
    const title = result.title;
    const years = result.years;
    const artist = result.artist;
    const email = localStorage.getItem("email");
    const image_name = `${artist.split(" ").join("")}.jpg`;
    const artistImgRes = await apiUtils.getArtistImg(image_name);
    console.log(image_name);
    const image_url = artistImgRes.data;
    console.log(email, title, artist, years,image_url);
     apiUtils.subscribeSong({email,title,years,artist,image_url})
  }



  const { title, artist, years,image_url } = result;
  return (
      <tr>
        <td>{title}</td>
        <td>{years}</td>
        <td>{artist}</td>
        <td><img src={image_url} alt={artist} /></td>
        <button onClick={() => handleSubscribe(result)}>Subscribe</button>
      </tr>
  );
};

export default MusicItem;
