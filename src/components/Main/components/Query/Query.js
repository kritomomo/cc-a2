import React from 'react';
import * as apiUtils from '../../../../utils/apiUtils';
import "./Query.css";
// import MusicItem from './MusicItem';

const Query = ({show, setShow}) => {
  const [state, setState] = React.useState({
    title: "",
    artist: "",
    year: "",
  });

  const [queryRes, setQueryRes] = React.useState({
    queryResult: [],
  })

  const [queryValid, setQueryValid] = React.useState(false);

  const handleChange = (e) =>{
    const value = e.target.value;
    setState({
      ...state,
      [e.target.name]: value
    });
  }

  const handleQuery = async (e) => {
    e.preventDefault();
    const title = state.title;
    const artist = state.artist;
    const year = state.year;
    if (title.length === 0 && artist.length === 0 && year.length === 0) {
      setQueryValid(true);
      setQueryRes({
        queryResult: [],
      })
    
    } else {
      try {
        const queryRes = await apiUtils.querySong({ title, artist, year });
        // const artistImgRes = await apiUtils.getArtistImg(artist);
        if (queryRes.status === 200) {
          setQueryRes({
            queryResult: queryRes.data,
          })
          setQueryValid(false);
         
        }
      } catch (error) {
        if (error.response.status === 404) {
          setQueryValid(true);
          setQueryRes({
            queryResult: [],
          })
        }
      }
    }
   
  }

  const handleSubscribe = async (result) => {
    const title = result.title;
    const years = result.years;
    const artist = result.artist;
    const email = localStorage.getItem("email");
    const image_name = `${artist.split(" ").join("")}.jpg`;
    const artistImgRes = await apiUtils.getArtistImg(image_name);
    // console.log(image_name);
    const image_url = artistImgRes.data;
    // console.log(email, title, artist, years,image_url);
    const addRes = await apiUtils.subscribeSong({ email, title, years, artist, image_url })
    console.log(addRes.status);
    if (addRes.status === 200) {
      setShow(show + 1);
    }
  }

  // const queryDisplay = queryRes.queryResult[0] ? (queryRes.queryResult.map((e) => <MusicItem key={e} result={e} />)) : <div></div>
  const { queryResult } = queryRes;

  return (
    <div className="query-layout">
      {queryValid ? <div style={{color: 'red'}}>No result is retrieved. Please query again</div> : null}
      <form onSubmit={handleQuery}>
        <label>
            Title:
            <input
              type="text"
              name="title"
              value={state.title}
              onChange={handleChange}
            />
        </label>
        <label>
            Year:
            <input
              type="text"
              name="year"
              value={state.year}
              onChange={handleChange}
            />
        </label>
        <label>
            Artist:
            <input
              type="text"
              name="artist"
              value={state.artist}
              onChange={handleChange}
            />
        </label>
        <button >Query</button>
      </form>
      <div className="query_results">
        <table>
          <tr>
            <th>Title</th>
            <th>Year</th>
            <th>Artist</th>
            <th>Artist Image</th>
          </tr>
          {/* {queryDisplay} */}
          {queryResult.map((result,index) => (
            <tr>
              <td>{result.title}</td>
              <td>{result.years}</td>
              <td>{result.artist}</td>
              <td><img src={result.image_url} alt={result.artist} /></td>
              <button onClick={() => handleSubscribe(result,index)}>Subscribe</button>
            </tr>
          ))}
        </table>
      </div>
    </div>
  )
}

export default Query;

