import React from "react";
import {
  useHistory,
  Link,
} from 'react-router-dom';
import * as apiUtils from '../../utils/apiUtils';



const Login = () => {
  const [state, setState] = React.useState({
    email: "",
    password: "",
  });
  const [warning, setWarning] = React.useState(false);
  
  const history = useHistory();

  const handleChange = (e) =>{
    const value = e.target.value;
    setState({
      ...state,
      [e.target.name]: value
    });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = state.email;
    const password = state.password;
    try {
      const loginRes = await apiUtils.login({ email, password });
      console.log(email);
      console.log(loginRes);
      if (loginRes.status === 200) {
        localStorage.setItem('email', email);
        history.push('/main')
      }
    }
    catch (error) {
      if (error.response.status >= 404) {
        setWarning(true);
      }
    }
    // setWarning(true);
    // alert('email:' + email + ' password:' + password);
  }

  return (
    <div>
      {warning ? <div style={{color: 'red'}}>email or password is invalid</div> : null}
      <form onSubmit={handleSubmit}>
        <label>
          Email:
          <input
            type="text"
            name="email"
            value={state.email}
            onChange={handleChange}
          />
        </label>
        <label>
          Password:
          <input
            type="text"
            name="password"
            value={state.password}
            onChange={handleChange}
          />
        </label>
        <button >Login</button>
      </form>
      <Link to='/register'>Register</Link>
    </div>
  );
}

export default Login;
