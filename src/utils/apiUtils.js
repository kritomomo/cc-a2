import axios from "axios";

const baseUrl = 'http://ec2-54-89-28-172.compute-1.amazonaws.com:8000'

export const login = ({email, password}) => axios({
  method: 'get',
  url: `${baseUrl}/login`,
  params: {
    email,
    password,
  }
})

export const register = ({ email, user_name, password }) => axios({
  method: 'post',
  url: `${baseUrl}/register`,
  data: {
    email,
    user_name,
    password,
  }
})

export const getName = (email) => axios({
  method: 'get',
  url: `${baseUrl}/name`,
  params: {
    email
  }
})

export const getSubscriptionDetails = (email) => axios({
  method: 'get',
  url: `${baseUrl}/subscription`,
  params: {
    email
  }
})

//S3 
export const getArtistImg = (artist) => axios({
  method: 'get',
  url: `${baseUrl}/artistImg`,
  params: {
    artist
  }
})

// delete subscription
export const deleteSubscription = ({ title,email }) => axios({
  method: 'post',
  url: `${baseUrl}/delete`,
  data: {
    title,
    email
  }
})

// query music
export const querySong = ({ title, year, artist }) => axios({
  method: 'get',
  url: `${baseUrl}/musicList`,
  params: {
    title,
    year,
    artist,
  }
})

// subscribe song 
export const subscribeSong = ({ email, title, years, artist,image_url }) => axios({
  method: 'post',
  url: `${baseUrl}/subscribeList`,
  data: {
    email,
    title,
    years,
    artist,
    image_url
  }
})
